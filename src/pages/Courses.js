import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	
	//Check to see if the mock data was captured
	//console.log(courseData);
	//console.log(courseData[0]);

	/*
		Props
			is a shorthand for "property" since components are considered as object in ReactJS

			is a way to pass data from a parent component to a child component

			it is synonymous to the function parameter

			This is referred to as "props drilling"
	*/

	const courses = courseData.map(course => {
		return(
			<CourseCard courseProp={course} key={course.id}/>
		)
	})

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}