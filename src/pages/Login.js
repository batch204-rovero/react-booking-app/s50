import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Login() {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [canLogin, setCanLogin] = useState(false);

	useEffect(() => {
		if (email !== '' && password !== '') {
			setCanLogin(true);
		} else {
			setCanLogin(false);
		}
	}, [email, password]);

	function loginUser(e) {
		e.preventDefault(); //prevent default form behavior, so that the form does not submit

		setEmail("");
		setPassword("");
		
		alert("Thank you for logging in");
	};

	return(
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label></Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label></Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{canLogin ?
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
				Submit
				</Button>
				:
				<Button className="mt-3" variant="primary" id="submitBtn" disabled>
				Submit
				</Button>				
			}
		</Form>
	)
}